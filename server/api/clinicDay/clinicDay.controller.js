'use strict';

var _ = require('lodash');
var Clinicday = require('./clinicDay.model');
var Midwife = require('../midwife/midwife.model');
var Transferreason = require('../transferReason/transferReason.model');
var moment = require('moment');
var fs = require('fs')

var createDate = function(year, month, date) {
  return new Date(Date.UTC(year, month, date, 0, 0, 0, 0));
};

// Get list of clinicDays
exports.index = function(req, res) {
  Clinicday.find()
  //.populate('births')
  .exec(function (err, clinicDays) {
    if(err) { return handleError(res, err); }
    return res.json(200, clinicDays);
  });
};

// Get a single clinicDay
exports.show = function(req, res) {
  Clinicday.findById(req.params.id)
  //.populate('births')
  .exec(function (err, clinicDay) {
    if(err) { return handleError(res, err); }
    if(!clinicDay) { return res.send(404); }
    return res.json(clinicDay);
  });
};

// Get a single clinicDay by date
// We treat this as a getOrCreate() as this should always return a clinic Day for a given date
exports.showByDate = function(req, res) {
  var date = createDate(req.query.year, req.query.month, req.query.date);
  var params = {date: date};
  Clinicday.findOne(params)
  //.populate('births')
  .exec(function (err, clinicDay) {
    if(err) { return handleError(res, err); }

    if(!clinicDay) {
      // No clinic day found, we need to create one
      Clinicday.create(params, function(err, newClinicDay) {
        if(err) { return handleError(res, err); }
        return res.json(newClinicDay);
      });
    } else {
      return res.json(clinicDay);
    }
  });
};


// Aggregate birth stats by dates
exports.aggregate = function(req, res) {
  var startDate = new Date(req.params.startDate);
  var endDate = new Date(req.params.endDate);
  var params = {date: {$gte:startDate, $lte: endDate}};
  var report = {};

  var stream = Clinicday.find(params).sort('date').stream();

  var extraColumns = {
    prenatal: [],
    postpartum: [],
    birth: [],
    midwife: [],
    extra: []
  };

  var currentRange = {
    start: startDate,
    end: endDate
  };
  var currentRangeStr = req.params.startDate + " to " + req.params.endDate;

  var generateCurrentRangeStr;
  if (req.query.grouping == 'annual') {
    generateCurrentRangeStr = function(date) {
      return moment(date).format('YYYY');
    };
  } else if (req.query.grouping == 'quarter') {
    generateCurrentRangeStr = function(date) {
      return moment(date).format('[Q]Q YYYY');
    };
  } else if (req.query.grouping == 'week') {
    generateCurrentRangeStr = function(date) {
      var isoWeekNo = moment(date).isoWeek();
      var monday = moment().day("Monday").isoWeek(isoWeekNo);
      var sunday = moment().day("Sunday").isoWeek(isoWeekNo);
      return monday.format('M/D/YYYY') + ' to ' + sunday.format('M/D/YYYY');
    };
  } else {
    generateCurrentRangeStr = function(date) {
      return moment(date).format('MMM YYYY');
    };
  }

  stream.on('data', function (doc) {

    // Check if out of group_by range with moment.js and initialize a new
    // report['date range prefix']
    currentRangeStr = generateCurrentRangeStr(doc.date);

    if (report[currentRangeStr] === undefined) {
      report[currentRangeStr] = {
        prenatalInitialVisits: 0,
        prenatalReturnVisits: 0,
        prenatalTotalTransfers: 0,
        prenatalTotalTransferMotherDemises: 0,
        prenatalTotalTransferBabyDemises: 0,

        postpartumInitialVisits: 0,
        postpartumReturnVisits: 0,
        postpartumTotalTransfers: 0,
        postpartumTotalTransferMotherDemises: 0,
        postpartumTotalTransferBabyDemises: 0,

        birthTotal: 0,
        birthTotalTransfers: 0,
        birthTotalTransferMotherDemises: 0,
        birthTotalTransferBabyDemises: 0,
      };
    }

    // Handle Transfers
    _.each(['prenatal', 'postpartum', 'birth'], function(prefix) {
      report[currentRangeStr][prefix + 'TotalTransfers'] += doc[prefix + 'Transfers'].length || 0;

      _.each(doc[prefix + 'Transfers'], function (transfer) {
        var columnTextPrefix = prefix + ' transfer "' + transfer.reason + '"';
        var columnTotalText = columnTextPrefix + ' total';
        var columnMotherText = columnTextPrefix + ' mother demises';
        var columnBabyText = columnTextPrefix + ' baby demises';
        if (report[currentRangeStr][columnTotalText] === undefined) {
          report[currentRangeStr][columnTotalText] = 0;
          report[currentRangeStr][columnMotherText] = 0;
          report[currentRangeStr][columnBabyText] = 0;
          extraColumns[prefix].push(columnTotalText, columnMotherText, columnBabyText)
        }

        report[currentRangeStr][columnTotalText] += 1;

        if (transfer.motherLived === false) {
          report[currentRangeStr][prefix + 'TotalTransferMotherDemises'] += 1;
          report[currentRangeStr][columnMotherText] += 1;
        }
        if (transfer.babyLived === false) {
          report[currentRangeStr][prefix + 'TotalTransferBabyDemises'] += 1
          report[currentRangeStr][columnBabyText] += 1;
        }
      });
    });


    // Prenatal Visits
    report[currentRangeStr]['prenatalInitialVisits'] += doc['prenatalInitialVisits'] || 0;
    report[currentRangeStr]['prenatalReturnVisits'] += doc['prenatalReturnVisits'] || 0;


    // Postpartum Visits
    report[currentRangeStr]['postpartumInitialVisits'] += doc['postpartumInitialVisits'] || 0;
    report[currentRangeStr]['postpartumReturnVisits'] += doc['postpartumReturnVisits'] || 0;

    // Birth data
    report[currentRangeStr]['birthTotal'] += doc['births'].length || 0;
    _.each(doc['births'], function (birth) {
      var primaryText = birth.primaryMidwife + ' primary';
      var primaryTextSecondary = birth.primaryMidwife + ' secondary';
      if (report[currentRangeStr][primaryText] === undefined) {
        report[currentRangeStr][primaryText] = 0;
        report[currentRangeStr][primaryTextSecondary] = 0;
        extraColumns['midwife'].push(primaryText, primaryTextSecondary);
      }
      report[currentRangeStr][primaryText] += 1;

      if (birth.secondaryMidwife) {
        var secondaryTextPrimary = birth.secondaryMidwife + ' primary';
        var secondaryText = birth.secondaryMidwife + ' secondary';
        if (report[currentRangeStr][secondaryText] === undefined) {
          report[currentRangeStr][secondaryText] = 0;
          report[currentRangeStr][secondaryTextPrimary] = 0;
          extraColumns['midwife'].push(secondaryTextPrimary, secondaryText);
        }
        report[currentRangeStr][secondaryText] += 1;
      }
    });

    // Extra Fields
    _.each(doc['extraFields'], function (field) {
      if (report[currentRangeStr][field.name] === undefined) {
        report[currentRangeStr][field.name] = 0;
        extraColumns['extra'].push(field.name);
      }
      report[currentRangeStr][field.name] += field.value;
    });

  }).on('error', function (err) {
    return handleError(res, err);
  }).on('close', function () {
    // generate CSV string from report
    var headers = ['prenatalInitialVisits','prenatalReturnVisits','prenatalTotalTransfers','prenatalTotalTransferMotherDemises','prenatalTotalTransferBabyDemises','postpartumInitialVisits','postpartumReturnVisits','postpartumTotalTransfers','postpartumTotalTransferMotherDemises','postpartumTotalTransferBabyDemises','birthTotal','birthTotalTransfers','birthTotalTransferMotherDemises','birthTotalTransferBabyDemises'];
    var orderedExtraFieldTypes = ['extra', 'birth', 'prenatal', 'postpartum', 'midwife'];
    _.each(orderedExtraFieldTypes, function(fieldType) {
      _.each(extraColumns[fieldType], function(field) {
        headers.push(field);
      });
    });

    var csv = 'range'
    var csv = 'range,' + headers.join(',');

    _.forOwn(report, function(reportRow, reportRange) {
      var rowText = '\n' + reportRange;

      _.each(headers, function(header) {
        if (header in reportRow) {
          rowText += ',' + reportRow[header];
        } else {
          rowText += ',';
        }
      });
      csv += rowText;
    });

    var filename = 'report.csv';
    var filepath = (+new Date()).toString(36);
    fs.writeFile(['/tmp', filepath + filename].join('/'), csv, function (err,data) {
      if (err) {
        return console.log(err);
      }
      return res.json({extraColumns: extraColumns,
                      report: report,
                      csv: csv,
                      url: ['/api/clinic-days/report', filepath, filename].join('/')
      });
    });

  });
};


// Allows for downloading a report and then deleting the file
exports.downloadReport = function (req, res) {
  var fullFilePath = ['/tmp', req.params.filepath + req.params.filename].join('/');
  var stream = fs.createReadStream(fullFilePath, {bufferSize: 64 * 1024})
  stream.pipe(res);

  var had_error = false;
  stream.on('error', function(err){
    had_error = true;
  });
  stream.on('close', function(){
    if (!had_error) fs.unlink(fullFilePath);
  });
};


// Creates a new clinicDay in the DB.
exports.create = function(req, res) {
  Clinicday.create(req.body, function(err, clinicDay) {
    if(err) { return handleError(res, err); }
    return res.json(201, clinicDay);
  });
};

// Updates an existing clinicDay in the DB.
exports.update = function(req, res) {
  Clinicday.findById(req.params.id).remove(function(err, clinicDay) {
    if (err) { return handleError(err); }
    var updated = new Clinicday(req.body);
    updated.save(function (err) {
      if (err) { return handleError(err); }
      return res.json(200, updated);
    });
  });
  /*
  if(req.body._id) { delete req.body._id; }
  Clinicday.findById(req.params.id)
  .exec(function (err, clinicDay) {
    if (err) { return handleError(err); }
    if(!clinicDay) { return res.send(404); }

    // Update Birth and all Transfer arrays everytime for now
    clinicDay.births = [];
    clinicDay.birthTransfers = [];
    clinicDay.prenatalTransfers = [];
    clinicDay.postpartumTransfers = [];
    var updated = _.merge(clinicDay, req.body);
    updated.markModified('births');
    updated.markModified('birthTransfers');
    updated.markModified('prenatalTransfers');
    updated.markModified('postpartumTransfers');
    updated.extraFields = [];
    updated.extraFields.push(req.body.extraFields);

    updated.save(function (err) {
      if (err) { return handleError(err); }
      return res.json(200, clinicDay);
    });
  });
  */
};

// Deletes a clinicDay from the DB.
exports.destroy = function(req, res) {
  Clinicday.findById(req.params.id, function (err, clinicDay) {
    if(err) { return handleError(res, err); }
    if(!clinicDay) { return res.send(404); }
    clinicDay.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};


// Finds all distinct values for different transfer reasons and midwives
exports.getChoices = function(req, res) {
  var choices = {
    prenatalTransferReasons: [],
    birthTransferReasons: [],
    postpartumTransferReasons: [],
    midwives: [],
    extraFields: []
  };

  Clinicday.distinct('prenatalTransfers.reason', function(err, data) {
    if(err) { return handleError(res, err); }
    choices.prenatalTransferReasons = data;
    Clinicday.distinct('birthTransfers.reason', function(err, data) {
      if(err) { return handleError(res, err); }
      choices.birthTransferReasons = data;
      Clinicday.distinct('postpartumTransfers.reason', function(err, data) {
        if(err) { return handleError(res, err); }
        choices.postpartumTransferReasons = data;
        Clinicday.distinct('births.primaryMidwife', function(err, data) {
          if(err) { return handleError(res, err); }
          choices.midwives = data;
          Clinicday.distinct('births.secondaryMidwife', function(err, data) {
            if(err) { return handleError(res, err); }
            choices.midwives = _.union(choices.midwives, data);
            Clinicday.distinct('extraFields.name', function(err, data) {
              if(err) { return handleError(res, err); }
              choices.extraFields = data;
              return res.json(200, choices);
            });
          });
        });
      });
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}
