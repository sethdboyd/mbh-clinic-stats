'use strict';

var express = require('express');
var controller = require('./clinicDay.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/report/:filepath/:filename', controller.downloadReport);
router.get('/choices', auth.hasRole('admin'), controller.getChoices);
router.get('/', auth.hasRole('admin'), controller.index);
router.get('/by-date', auth.hasRole('admin'), controller.showByDate);
router.get('/:id', auth.hasRole('admin'), controller.show);
router.post('/', auth.hasRole('admin'), controller.create);
router.put('/:id', auth.hasRole('admin'), controller.update);
router.patch('/:id', auth.hasRole('admin'), controller.update);
router.get('/aggregate/:startDate/:endDate', auth.hasRole('admin'), controller.aggregate);
//router.delete('/:id', auth.hasRole('admin'), controller.destroy);

module.exports = router;
