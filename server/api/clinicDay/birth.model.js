'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var BirthSchema = new Schema({
  _id: false,
  primaryMidwife: {type: Schema.Types.ObjectId, ref: 'Midwife', required: true},
  secondaryMidwife: {type: Schema.Types.ObjectId, ref: 'Midwife'}
});

module.exports.model = mongoose.model('Birth', BirthSchema);
module.exports.schema = BirthSchema;
