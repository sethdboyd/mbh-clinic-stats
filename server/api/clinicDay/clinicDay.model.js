'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
//var BirthSchema = require('./birth.model').schema;

var ClinicdaySchema = new Schema({
  date: {type: Date, required: true, unique: true},

  prenatalInitialVisits: Number,
  prenatalReturnVisits: Number,
  prenatalTransfers: [{
    reason: {type: String, index: true},
    motherLived: {type: Boolean, default: true},
    babyLived: {type: Boolean, default: true},
  }],

  postpartumInitialVisits: Number,
  postpartumReturnVisits: Number,
  postpartumTransfers: [{
    reason: {type: String, index: true},
    motherLived: {type: Boolean, default: true},
    babyLived: {type: Boolean, default: true},
  }],

  births: [{
    primaryMidwife: {type: String, index: true},
    secondaryMidwife: {type: String, index: true}
  }],
  birthTransfers: [{
    reason: {type: String, index: true},
    motherLived: {type: Boolean, default: true},
    babyLived: {type: Boolean, default: true},
  }],

  extraFields: [{
    name: {type: String, index: true},
    value: Number
  }]

});

ClinicdaySchema.pre('validate', true, function(next, done){
  var self = this; // save a reference to status
  next();  // start working on any other middleware in parallel with this one

  if (self.date) {
    // Put date in UTC and strip time information
    self.date = new Date(Date.UTC(self.date.getUTCFullYear(),
                                  self.date.getUTCMonth(),
                                  self.date.getUTCDate(),
                                  0, 0, 0, 0));
  }
  done();
});

module.exports = mongoose.model('Clinicday', ClinicdaySchema);
