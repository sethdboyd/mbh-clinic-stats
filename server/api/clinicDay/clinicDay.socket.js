/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Clinicday = require('./clinicDay.model');

exports.register = function(socket) {
  Clinicday.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Clinicday.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('clinicDay:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('clinicDay:remove', doc);
}