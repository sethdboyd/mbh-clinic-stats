/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Transferreason = require('./transferReason.model');

exports.register = function(socket) {
  Transferreason.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Transferreason.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('transferReason:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('transferReason:remove', doc);
}