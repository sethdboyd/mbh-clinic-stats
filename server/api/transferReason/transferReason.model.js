'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TransferreasonSchema = new Schema({
  name: String,
  info: String,
  active: Boolean
});

module.exports = mongoose.model('Transferreason', TransferreasonSchema);