'use strict';

var _ = require('lodash');
var Transferreason = require('./transferReason.model');

// Get list of transferReasons
exports.index = function(req, res) {
  Transferreason.find(function (err, transferReasons) {
    if(err) { return handleError(res, err); }
    return res.json(200, transferReasons);
  });
};

// Get a single transferReason
exports.show = function(req, res) {
  Transferreason.findById(req.params.id, function (err, transferReason) {
    if(err) { return handleError(res, err); }
    if(!transferReason) { return res.send(404); }
    return res.json(transferReason);
  });
};

// Creates a new transferReason in the DB.
exports.create = function(req, res) {
  Transferreason.create(req.body, function(err, transferReason) {
    if(err) { return handleError(res, err); }
    return res.json(201, transferReason);
  });
};

// Updates an existing transferReason in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Transferreason.findById(req.params.id, function (err, transferReason) {
    if (err) { return handleError(err); }
    if(!transferReason) { return res.send(404); }
    var updated = _.merge(transferReason, req.body);
    updated.save(function (err) {
      if (err) { return handleError(err); }
      return res.json(200, transferReason);
    });
  });
};

// Deletes a transferReason from the DB.
exports.destroy = function(req, res) {
  Transferreason.findById(req.params.id, function (err, transferReason) {
    if(err) { return handleError(res, err); }
    if(!transferReason) { return res.send(404); }
    transferReason.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}