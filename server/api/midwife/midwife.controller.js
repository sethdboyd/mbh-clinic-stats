'use strict';

var _ = require('lodash');
var Midwife = require('./midwife.model');

// Get list of midwifes
exports.index = function(req, res) {
  Midwife.find(function (err, midwifes) {
    if(err) { return handleError(res, err); }
    return res.json(200, midwifes);
  });
};

// Get a single midwife
exports.show = function(req, res) {
  Midwife.findById(req.params.id, function (err, midwife) {
    if(err) { return handleError(res, err); }
    if(!midwife) { return res.send(404); }
    return res.json(midwife);
  });
};

// Creates a new midwife in the DB.
exports.create = function(req, res) {
  Midwife.create(req.body, function(err, midwife) {
    if(err) { return handleError(res, err); }
    return res.json(201, midwife);
  });
};

// Updates an existing midwife in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Midwife.findById(req.params.id, function (err, midwife) {
    if (err) { return handleError(err); }
    if(!midwife) { return res.send(404); }
    var updated = _.merge(midwife, req.body);
    updated.save(function (err) {
      if (err) { return handleError(err); }
      return res.json(200, midwife);
    });
  });
};

// Deletes a midwife from the DB.
exports.destroy = function(req, res) {
  Midwife.findById(req.params.id, function (err, midwife) {
    if(err) { return handleError(res, err); }
    if(!midwife) { return res.send(404); }
    midwife.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.send(204);
    });
  });
};

function handleError(res, err) {
  return res.send(500, err);
}