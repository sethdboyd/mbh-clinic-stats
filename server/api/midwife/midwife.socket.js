/**
 * Broadcast updates to client when the model changes
 */

'use strict';

var Midwife = require('./midwife.model');

exports.register = function(socket) {
  Midwife.schema.post('save', function (doc) {
    onSave(socket, doc);
  });
  Midwife.schema.post('remove', function (doc) {
    onRemove(socket, doc);
  });
}

function onSave(socket, doc, cb) {
  socket.emit('midwife:save', doc);
}

function onRemove(socket, doc, cb) {
  socket.emit('midwife:remove', doc);
}