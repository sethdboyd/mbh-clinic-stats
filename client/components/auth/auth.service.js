// Generated by CoffeeScript 1.7.1
(function() {
  'use strict';
  angular.module('mbhApp').factory('Auth', function($location, $rootScope, $http, User, $cookieStore, $q) {
    var currentUser;
    currentUser = {};
    if ($cookieStore.get('token')) {
      currentUser = User.get();
    }
    return {

      /*
      Authenticate user and save token
      
      @param  {Object}   user     - login info
      @param  {Function} callback - optional
      @return {Promise}
       */
      login: function(user, callback) {
        var cb, deferred;
        cb = callback || angular.noop;
        deferred = $q.defer();
        $http.post('/auth/local', {
          email: user.email,
          password: user.password
        }).success(function(data) {
          $cookieStore.put('token', data.token);
          currentUser = User.get();
          deferred.resolve(data);
          return cb();
        }).error((function(err) {
          this.logout();
          deferred.reject(err);
          return cb(err);
        }).bind(this));
        return deferred.promise;
      },

      /*
      Delete access token and user info
      
      @param  {Function}
       */
      logout: function() {
        $cookieStore.remove('token');
        currentUser = {};
      },

      /*
      Create a new user
      
      @param  {Object}   user     - user info
      @param  {Function} callback - optional
      @return {Promise}
       */
      createUser: function(user, callback) {
        var cb;
        cb = callback || angular.noop;
        return User.save(user, function(data) {
          $cookieStore.put('token', data.token);
          currentUser = User.get();
          return cb(user);
        }, (function(err) {
          this.logout();
          return cb(err);
        }).bind(this)).$promise;
      },

      /*
      Change password
      
      @param  {String}   oldPassword
      @param  {String}   newPassword
      @param  {Function} callback    - optional
      @return {Promise}
       */
      changePassword: function(oldPassword, newPassword, callback) {
        var cb;
        cb = callback || angular.noop;
        return User.changePassword({
          id: currentUser._id
        }, {
          oldPassword: oldPassword,
          newPassword: newPassword
        }, function(user) {
          return cb(user);
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      /*
      Change role
      
      @param  {String}   newRole
      @param  {Function} callback    - optional
      @return {Promise}
       */
      changeRole: function(newRole, callback) {
        var cb;
        cb = callback || angular.noop;
        return User.changeRole({
          id: currentUser._id
        }, {
          newRole: newRole
        }, function(user) {
          return cb(user);
        }, function(err) {
          return cb(err);
        }).$promise;
      },

      /*
      Gets all available info on authenticated user
      
      @return {Object} user
       */
      getCurrentUser: function() {
        return currentUser;
      },

      /*
      Check if a user is logged in
      
      @return {Boolean}
       */
      isLoggedIn: function() {
        return currentUser.hasOwnProperty('role');
      },

      /*
      Check if a user is an admin
      
      @return {Boolean}
       */
      isAdmin: function() {
        return currentUser.role === 'admin';
      },

      /*
      Get auth token
       */
      getToken: function() {
        return $cookieStore.get('token');
      }
    };
  });

}).call(this);
