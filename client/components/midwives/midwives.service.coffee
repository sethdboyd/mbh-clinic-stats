'use strict'

angular.module('mbhApp').factory 'Midwives', ($resource, $rootScope, socket) ->
  # Service logic
  midwivesResource = $resource '/api/midwives/:id',
    id: '@_id'
  ,
    update:
      method: 'PUT'

  addMidwife = (newMidwife) ->
    return if newMidwife is ''
    newMidwife = new midwivesResource({name: newMidwife, active: true})
    newMidwife.$save()

  deleteMidwife = (midwife) ->
    midwife.$remove()

  saveMidwife = (midwife) ->
    midwife.$update()

  factory =
    resource: midwivesResource
    list: midwivesResource.query()
    addMidwife: addMidwife
    saveMidwife: saveMidwife
    deleteMidwife: deleteMidwife

  socket.syncUpdates 'midwife', factory.list, (event, item, array) ->
    index = array.indexOf(item)
    array[index] = new midwivesResource(item)

  # Public API here
  return factory
