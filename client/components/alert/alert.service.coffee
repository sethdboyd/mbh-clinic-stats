'use strict'

angular.module 'mbhApp'
.factory 'Alert', ($timeout) ->
  alerts = []

  add = (type, msg) ->
    alert = { type: type, msg: msg, close: -> closeAlert(this) }
    alerts.push(alert)
    if type != 'error'
      $timeout( ->
        closeAlert(alert)
      , 5000)

  closeAlertIdx = (index) ->
    alerts.splice(index, 1)

  closeAlert = (alert) ->
    closeAlertIdx(alerts.indexOf(alert))

  factory =
    list: alerts
    add: add
    close: closeAlert

  # Public API here
  return factory
