'use strict'

angular.module('mbhApp').factory 'TransferReasons', ($resource, $rootScope, socket) ->
  transferReasonsResource = $resource '/api/transfer-reasons/:id',
    id: '@_id'
  ,
    update:
      method: 'PUT'

  addTransferReason = (newTransferReason) ->
    return if newTransferReason is ''
    newTransferReason = new transferReasonsResource({name: newTransferReason, active: true})
    newTransferReason.$save()

  deleteTransferReason = (transferReason) ->
    transferReason.$remove()

  saveTransferReason = (transferReason) ->
    transferReason.$update()

  factory =
    resource: transferReasonsResource
    list: transferReasonsResource.query()
    addTransferReason: addTransferReason
    saveTransferReason: saveTransferReason
    deleteTransferReason: deleteTransferReason

  socket.syncUpdates 'transferReason', factory.list, (event, item, array) ->
    index = array.indexOf(item)
    array[index] = new transferReasonsResource(item)

  # Public API here
  return factory
