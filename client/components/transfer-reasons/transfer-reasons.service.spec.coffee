'use strict'

describe 'Service: midwives', ->

  # load the service's module
  beforeEach module('mbhApp')

  # instantiate service
  midwives = undefined
  beforeEach inject((_midwives_) ->
    midwives = _midwives_
  )
  it 'should do something', ->
    expect(!!midwives).toBe true