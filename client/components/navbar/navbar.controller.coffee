'use strict'

angular.module('mbhApp').controller 'NavbarCtrl', ($scope, $location, Auth, Alert) ->
  $scope.menu = [
    title: 'Home'
    link: '/'
  ]
  $scope.adminMenu = [
    {
      title: 'Daily Stats'
      link: '/clinic',
    },{
      title: 'Generate Reports'
      link: '/report',
    },
    {
      title: 'Users'
      link: '/admin'
    }
  ]

  $scope.isCollapsed = true
  $scope.isLoggedIn = Auth.isLoggedIn
  $scope.isAdmin = Auth.isAdmin
  $scope.getCurrentUser = Auth.getCurrentUser

  $scope.logout = ->
    Auth.logout()
    $location.path '/login'

  $scope.isActive = (route) ->
    route is $location.path()
