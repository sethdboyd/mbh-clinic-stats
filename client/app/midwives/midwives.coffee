'use strict'

angular.module('mbhApp')
  .config ($stateProvider) ->
    $stateProvider
    .state('midwives',
      url: '/midwives',
      templateUrl: 'app/midwives/midwives.html'
      controller: 'MidwivesCtrl'
    )
