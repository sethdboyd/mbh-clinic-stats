'use strict'

describe 'Controller: MidwivesCtrl', ->

  # load the controller's module
  beforeEach module('mbhApp')
  MidwivesCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject(($controller, $rootScope) ->
    scope = $rootScope.$new()
    MidwivesCtrl = $controller('MidwivesCtrl',
      $scope: scope
    )
  )
  it 'should ...', ->
    expect(1).toEqual 1
