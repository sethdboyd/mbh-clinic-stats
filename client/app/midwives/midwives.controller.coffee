'use strict'

angular.module('mbhApp').controller 'MidwivesCtrl', ($scope, Midwives) ->
  $scope.midwives = Midwives.list
  midwivesResource = Midwives.resource

  $scope.editedMidwife = null

  $scope.editMidwife = (midwife) ->
    $scope.editedMidwife = midwife
    $scope.originalMidwife = angular.extend({}, midwife)

  $scope.doneEditing = (midwife) ->
    $scope.editedMidwife = null
    midwife.name = midwife.name.trim()

    if !midwife.name
      $scope.deleteMidwife(midwife)
    else
      $scope.saveMidwife(midwife)

  $scope.revertEditing = (midwife) ->
    $scope.midwives[$scope.midwives.indexOf(midwife)] = $scope.originalMidwife
    $scope.doneEditing($scope.originalMidwife)

  $scope.saveMidwife = (midwife) ->
    Midwives.saveMidwife(midwife)

  $scope.deleteMidwife = (midwife) ->
    Midwives.deleteMidwife(midwife)

  $scope.addMidwife = (newMidwife) ->
    Midwives.addMidwife(newMidwife)
