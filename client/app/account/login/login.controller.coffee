'use strict'

angular.module('mbhApp').controller 'LoginCtrl',
($scope, $location, $window, Auth) ->
  $scope.user = {email: 'admin@admin.com', password: 'admin'}
  $scope.errors = {}
  $scope.login = (form) ->
    $scope.submitted = true

    if form.$valid
      # Logged in, redirect to home
      Auth.login(
        email: $scope.user.email
        password: $scope.user.password
      ).then(->
        $location.path '/clinic'
      ).catch (err) ->
        $scope.errors.other = err.message

  $scope.loginOauth = (provider) ->
    $window.location.href = '/auth/' + provider
