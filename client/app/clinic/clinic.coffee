'use strict'

angular.module('mbhApp').config ($stateProvider) ->
  $stateProvider.state 'clinic',
    url: '/clinic'
    templateUrl: 'app/clinic/clinic.html'
    controller: 'ClinicCtrl'
