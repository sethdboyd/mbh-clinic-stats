'use strict'

describe 'Controller: ClinicCtrl', ->

  # load the controller's module
  beforeEach module('mbhApp')
  ClinicCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject(($controller, $rootScope) ->
    scope = $rootScope.$new()
    ClinicCtrl = $controller('ClinicCtrl',
      $scope: scope
    )
  )
  it 'should ...', ->
    expect(1).toEqual 1
