'use strict'

angular.module('mbhApp').controller 'ClinicCtrl', ($scope, $http, $filter, Alert) ->

  $scope.newBirth = primaryMidwife: null, secondaryMidwife: null
  $scope.newBirthTransfer = reason: null, motherLived: true, babyLived: true
  $scope.newPrenatalTransfer = reason: null, motherLived: true, babyLived: true
  $scope.newPostpartumTransfer = reason: null, motherLived: true, babyLived: true
  $scope.newExtraField = name: '', value: 0

  $scope.clinicDay = null
  $scope.newClinicDay = new Date().toISOString().substring(0, 10)
  $scope.indices = {}
  $scope.choices = {
    prenatalTransferReasons: [],
    birthTransferReasons: [],
    postpartumTransferReasons: [],
    midwives: [],
    extraFields: []
  }

  $scope.changeClinicDay = ->
    return if $scope.newClinicDay is ''

    newClinicDay = new Date($scope.newClinicDay)
    config =
      params:
        year: newClinicDay.getUTCFullYear()
        month: newClinicDay.getUTCMonth()
        date: newClinicDay.getUTCDate()
    $http.get('/api/clinic-days/by-date', config).success (clinicDay) ->
      $scope.clinicDay = clinicDay

  $scope.saveClinicDay = ->
    return if $scope.clinicDay is null

    $http.put('/api/clinic-days/' + $scope.clinicDay._id, $scope.clinicDay)
    .success (clinicDay) ->
      Alert.add('success', 'Everything was successfully saved')
      $scope.clinicDay = clinicDay
    .error (err) ->
      Alert.add('error', err)

  $scope.updateChoices = ->
    $http.get('/api/clinic-days/choices')
    .success (choices) ->
      $scope.choices = choices
    .error (err) ->
      Alert.add('error', err)

  $scope.addBirth = ->
    if $scope.clinicDay.births is undefined
      $scope.clinicDay.births = []
    $scope.clinicDay.births.push(
      primaryMidwife: $scope.newBirth.primaryMidwife
      secondaryMidwife: $scope.newBirth.secondaryMidwife
    )

  $scope.removeBirth = (index) ->
    $scope.clinicDay.births.splice(index, 1)

  $scope.addTransfer = (newTransfer, transferArrName) ->
    if $scope.clinicDay[transferArrName] is undefined
      $scope.clinicDay[transferArrName] = []
    $scope.clinicDay[transferArrName].push(angular.copy(newTransfer))
    $scope.indices[transferArrName] = $scope.clinicDay[transferArrName].length - 1

  $scope.removeTransfer = (index, transferArrName) ->
    $scope.clinicDay[transferArrName].splice(index, 1)

  $scope.addExtraField = ->
    if $scope.clinicDay.extraFields is undefined
      $scope.clinicDay.extraFields = []
    $scope.clinicDay.extraFields.push(
      name: $scope.newExtraField.name
      value: $scope.newExtraField.value
    )
    $scope.indices.extraFields = $scope.clinicDay.extraFields.length - 1

  $scope.removeExtraField = (index) ->
    $scope.clinicDay.extraFields.splice(index, 1)

  $scope.checkChoices = (value, choiceType) ->
    if $scope.choices[choiceType].indexOf(value) == -1
      $scope.choices[choiceType].push(value)

  $scope.changeClinicDay()
  $scope.updateChoices()

