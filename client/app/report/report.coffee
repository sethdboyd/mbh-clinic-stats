'use strict'

angular.module('mbhApp').config ($stateProvider) ->
  $stateProvider.state 'report',
    url: '/report'
    templateUrl: 'app/report/report.html'
    controller: 'ReportCtrl'
