'use strict'

angular.module('mbhApp').controller 'ReportCtrl',
($scope, $http, $window) ->
  $scope.groupingOptions = [
    {id: 'week', display: 'Weekly'},
    {id: 'month', display: 'Monthly'},
    {id: 'quarter', display: 'Quarterly'},
    {id: 'annual', display: 'Annually'}
  ]
  $scope.grouping = 'week'


  $scope.clinicDay = null
  $scope.report = ''
  $scope.reportUrl = null
  $scope.reportStartDate = new Date().toISOString().substring(0, 10)
  $scope.reportEndDate = new Date().toISOString().substring(0, 10)

  $scope.generateReport = ->
    return if $scope.reportStartDate is ''
    return if $scope.reportEndDate is ''

    config =
      params:
        grouping: $scope.grouping

    $http.get('/api/clinic-days/aggregate/' +
      $scope.reportStartDate + '/' +
      $scope.reportEndDate,
      config
    ).success (report) ->
      $scope.report = report
      $window.location = report.url
