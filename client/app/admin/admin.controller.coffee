'use strict'

angular.module('mbhApp').controller 'AdminCtrl', ($scope, $http, Auth, User) ->
  $http.get('/api/users').success (users) ->
    $scope.users = users

  $scope.delete = (user) ->
    User.remove id: user._id
    angular.forEach $scope.users, (u, i) ->
      $scope.users.splice i, 1  if u is user

  $scope.toggleAdmin = (user) ->
    if user.role == 'admin'
      user.role = 'user'
    else
      user.role = 'admin'
    newRole = user.role
    User.changeRole
      id: user._id
    ,
      newRole: newRole

  $scope.isAdmin = (user) ->
    return user.role == 'admin'
