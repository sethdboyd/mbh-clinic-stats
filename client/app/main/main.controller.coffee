'use strict'

angular.module('mbhApp').controller 'MainCtrl', ($scope, Auth) ->
  $scope.isLoggedIn = Auth.isLoggedIn
