'use strict'

angular.module('mbhApp').controller 'TransferReasonsCtrl', ($scope, TransferReasons) ->
  $scope.transferReasons = TransferReasons.list
  transferReasonsResource = TransferReasons.resource

  $scope.editedTransferReason = null

  $scope.editTransferReason = (transferReason) ->
    $scope.editedTransferReason = transferReason
    $scope.originalTransferReason = angular.extend({}, transferReason)

  $scope.doneEditing = (transferReason) ->
    $scope.editedTransferReason = null
    transferReason.name = transferReason.name.trim()

    if !transferReason.name
      $scope.deleteTransferReason(transferReason)
    else
      $scope.saveTransferReason(transferReason)

  $scope.revertEditing = (transferReason) ->
    $scope.transferReasons[$scope.transferReasons.indexOf(transferReason)] = $scope.originalTransferReason
    $scope.doneEditing($scope.originalTransferReason)

  $scope.saveTransferReason = (transferReason) ->
    TransferReasons.saveTransferReason(transferReason)

  $scope.deleteTransferReason = (transferReason) ->
    TransferReasons.deleteTransferReason(transferReason)

  $scope.addTransferReason = (newTransferReason) ->
    TransferReasons.addTransferReason(newTransferReason)
