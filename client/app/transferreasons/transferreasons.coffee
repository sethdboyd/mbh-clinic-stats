'use strict'

angular.module('mbhApp').config ($stateProvider) ->
  $stateProvider.state 'transferreasons',
    url: '/transferreasons'
    templateUrl: 'app/transferreasons/transferreasons.html'
    controller: 'TransferReasonsCtrl'
