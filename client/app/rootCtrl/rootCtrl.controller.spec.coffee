'use strict'

describe 'Controller: RootctrlCtrl', ->

  # load the controller's module
  beforeEach module 'mbhApp'
  RootctrlCtrl = undefined
  scope = undefined

  # Initialize the controller and a mock scope
  beforeEach inject ($controller, $rootScope) ->
    scope = $rootScope.$new()
    RootctrlCtrl = $controller 'RootctrlCtrl',
      $scope: scope

  it 'should ...', ->
    expect(1).toEqual 1
