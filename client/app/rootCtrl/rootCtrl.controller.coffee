'use strict'

angular.module 'mbhApp'
.controller 'RootCtrl', ($scope, Alert) ->
  $scope.alerts = Alert.list
